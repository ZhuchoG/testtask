//
//  AppDelegate.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)setNetworkActivityIndicatorVisible:(BOOL)setVisible;

@end

