//
//  CitiesViewTVC.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "CitiesViewTVC.h"
#import "CityTVCell.h"
#import "DetailedWeatherForCityVC.h"
#import "ForecastWeatherForCityTVC.h"
#import "SegmentedController.h"

@interface CitiesViewTVC () <UITextFieldDelegate>
@property (nonatomic, strong) UITextField *input;
@property (nonatomic, strong) UILabel *titleView;
@end

@implementation CitiesViewTVC

- (UITextField *)input
{
    if (!_input)
    {
        _input = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 280, 44)];
        _input.textAlignment = NSTextAlignmentCenter;
        self.input.placeholder = @"Input city name";
        _input.delegate = self;
        _input.returnKeyType = UIReturnKeyGo;
        [_input addTarget:self
                  action:@selector(addCity)
        forControlEvents:UIControlEventEditingDidEndOnExit];
    }
    return _input;
}

- (UILabel *)titleView
{
    if (!_titleView)
    {
        _titleView= [[UILabel alloc] init];
        _titleView.text = @"Weather";
        [_titleView sizeToFit];
    }
    return _titleView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButton)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.fetchedResultsController = [City MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"name" ascending:YES];
    
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.titleView = self.titleView;
}

- (void)addButton
{
    self.navigationItem.titleView = self.input;
    [self.input becomeFirstResponder];
}

- (void)addCity
{
    dispatch_queue_t fetchQ = dispatch_queue_create("Weather fetch", NULL);
    dispatch_async(fetchQ, ^{
        
        NSDictionary *data = [WeatherAPI getCurrentDataForCityName:self.input.text];
        
        dispatch_async(dispatch_get_main_queue(),^{
            
            if ([data[@"cod"] isEqual:@200])
            {
                [City cityWithDict:data];
                
                self.input.text = @"";
                self.input.placeholder = @"Input city name";
                self.navigationItem.titleView = self.titleView;
            }
            
            else
            {
                self.input.text = @"";
                self.input.placeholder = @"City not found";
                [self.input becomeFirstResponder];
            }
            
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        });
    });
}

- (IBAction)refresh
{
    [self.refreshControl beginRefreshing];
    
    dispatch_queue_t fetchQ = dispatch_queue_create("Weather fetch", NULL);
    dispatch_async(fetchQ, ^{
        
        NSMutableArray *dataArray = [NSMutableArray array];
        
        if (self.fetchedResultsController.fetchedObjects.count == 0)
        {
            NSDictionary *data = [WeatherAPI getCurrentDataForCityName:@"Moscow"];
            [dataArray addObject:data];
            data = [WeatherAPI getCurrentDataForCityName:@"Abakan"];
            [dataArray addObject:data];
        }
        else
        {
            for (City *city in self.fetchedResultsController.fetchedObjects)
            {
                NSDictionary *data = [WeatherAPI getCurrentDataForCityName:city.name];
                [dataArray addObject:data];
            }

        }
        dispatch_async(dispatch_get_main_queue(),^{
            NSLog(@"%@", dataArray);
            
            for (NSDictionary *data in dataArray)
            {
                [City cityWithDict:data];
            }
            
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        });
    });

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityTVCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cityCell"];
    
    City *city = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSDictionary *weather = [NSKeyedUnarchiver unarchiveObjectWithData:city.currentWeather];
    
    NSNumber *temp = weather[@"main"][@"temp"];
    temp = [NSNumber numberWithDouble:temp.doubleValue];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.maximumFractionDigits = 1;
    
    NSString *tempText = [NSString stringWithFormat:@"%@ ℃", [f stringFromNumber:temp]];
    
    cell.cityLabel.text = city.name;
    cell.tempLabel.text = tempText;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        City *city = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [city MR_deleteEntity];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    City *city = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    DetailedWeatherForCityVC *detailed = [[DetailedWeatherForCityVC alloc] initWithNibName:@"DetailedWeatherForCityVC" bundle:nil];
    ForecastWeatherForCityTVC *forecast = [[ForecastWeatherForCityTVC alloc] initWithNibName:@"ForecastWeatherForCityTVC" bundle:nil];
    detailed.city = city;
    forecast.city = city;
    SegmentedController *segmented = [[SegmentedController alloc] initWithKeys:@[@"Detail", @"Forecast"] forControllers:@{@"Detail": detailed, @"Forecast": forecast}];
    [self.navigationController pushViewController:segmented animated:YES];
}

@end
