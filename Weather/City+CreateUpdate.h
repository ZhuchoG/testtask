//
//  City+CreateUpdate.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "City.h"

@interface City (CreateUpdate)
+ (City *)cityWithDict:(NSDictionary *)dict;
@end
