//
//  City+CreateUpdate.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "City+CreateUpdate.h"

@implementation City (CreateUpdate)
+ (City *)cityWithDict:(NSDictionary *)dict
{
    NSNumber *identifier = dict[@"name"]?dict[@"name"]:dict[@"city"][@"name"];
    
    if (!identifier)
    {
        return nil;
    }
    
    City *city = nil;
    
    NSArray *matches = [City MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"name == %@", identifier]];
    
    if (!matches || ([matches count] > 1))
    {
        // handle error
    }
    else if (![matches count])
    {
        city = [City MR_createEntity];
        city.identifier = dict[@"id"];
        city.name = dict[@"name"];
        if (dict[@"list"])
        {
            city.weather =  [NSKeyedArchiver archivedDataWithRootObject:dict[@"list"]];
        }
        else
        {
            city.currentWeather = [NSKeyedArchiver archivedDataWithRootObject:dict];
        }
    }
    else
    {
        city = [matches lastObject];
        if (dict[@"list"])
        {
            city.weather =  [NSKeyedArchiver archivedDataWithRootObject:dict[@"list"]];
        }
        else
        {
            city.currentWeather = [NSKeyedArchiver archivedDataWithRootObject:dict];
        }
    }
    
    return city;
}
@end
