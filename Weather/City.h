//
//  City.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface City : NSManagedObject

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * weather;
@property (nonatomic, retain) NSData * currentWeather;

@end
