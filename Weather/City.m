//
//  City.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "City.h"


@implementation City

@dynamic identifier;
@dynamic name;
@dynamic weather;
@dynamic currentWeather;

@end
