//
//  GunsCoreDataTVC.h
//  Guns.ru-Reader
//
//  Created by Vitaliy Zhukov on 21.04.13.
//  Copyright (c) 2013 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CoreDataTVC : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

- (void)performFetch;
@property (nonatomic) BOOL suspendAutomaticTrackingOfChangesInManagedObjectContext;
@property BOOL debug;
@end
