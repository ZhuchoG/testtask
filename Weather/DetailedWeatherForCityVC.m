//
//  DetailedWeatherForCityVC.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "DetailedWeatherForCityVC.h"

@interface DetailedWeatherForCityVC ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunriseLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunsetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cloudsLabel;
@property (weak, nonatomic) IBOutlet UILabel *windspeedLabel;
@end

@implementation DetailedWeatherForCityVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.nameLabel.text = self.city.name;
    
    [self updateView];
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updateView
{
    NSDictionary *weather = [NSKeyedUnarchiver unarchiveObjectWithData:self.city.currentWeather];
    NSNumber *temp = weather[@"main"][@"temp"];
    temp = [NSNumber numberWithDouble:temp.doubleValue];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.maximumFractionDigits = 1;
    
    NSString *tempText = [NSString stringWithFormat:@"%@ ℃", [f stringFromNumber:temp]];
    
    self.tempLabel.text = tempText;
    
    self.descriptionLabel.text = weather[@"weather"][0][@"description"];
    
    self.pressLabel.text = [NSString stringWithFormat:@"Pressure: %@ hPa", [f stringFromNumber:weather[@"main"][@"pressure"]]];
    self.humidityLabel.text = [NSString stringWithFormat:@"Humidity: %@%%", weather[@"main"][@"humidity"]];
    self.cloudsLabel.text = [NSString stringWithFormat:@"Cloudiness: %@%%", weather[@"clouds"][@"all"]];
    self.windspeedLabel.text = [NSString stringWithFormat:@"Wind speed: %@ m/s", weather[@"wind"][@"speed"]];
    
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    
    NSNumber *timestamp = weather[@"sys"][@"sunrise"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp floatValue]];
    self.sunriseLabel.text = [NSString stringWithFormat:@"Sunrise: %@", [dateformatter stringFromDate:date]];
    timestamp = weather[@"sys"][@"sunset"];
    date = [NSDate dateWithTimeIntervalSince1970:[timestamp floatValue]];
    self.sunsetLabel.text = [NSString stringWithFormat:@"Sunset: %@", [dateformatter stringFromDate:date]];
}

- (void)refresh
{
    dispatch_queue_t fetchQ = dispatch_queue_create("Weather fetch", NULL);
    dispatch_async(fetchQ, ^{
        
        NSDictionary *data = [WeatherAPI getCurrentDataForCityName:self.city.name];
        NSDictionary *dataForecast = [WeatherAPI getForecastDataForCityName:self.city.name];
        
        dispatch_async(dispatch_get_main_queue(),^{
            NSLog(@"%@", data);
            NSLog(@"%@", dataForecast);
            
            [City cityWithDict:data];
            [City cityWithDict:dataForecast];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dataUpdated" object:nil];
            
            [self updateView];
        });
    });
}

@end
