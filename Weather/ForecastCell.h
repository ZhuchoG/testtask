//
//  ForecastCell.h
//  Weather
//
//  Created by Vitaliy Zhukov on 17.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabelDay;
@property (weak, nonatomic) IBOutlet UILabel *tempLabelNight;

- (void)updateWithDict:(NSDictionary *)dict;
@end
