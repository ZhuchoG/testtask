//
//  ForecastCell.m
//  Weather
//
//  Created by Vitaliy Zhukov on 17.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "ForecastCell.h"

@implementation ForecastCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithDict:(NSDictionary *)dict
{
    self.descriptionLabel.text = dict[@"weather"][0][@"description"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.maximumFractionDigits = 1;
    
    NSNumber *temp = dict[@"temp"][@"day"];
    temp = [NSNumber numberWithDouble:temp.doubleValue];
    
    self.tempLabelDay.text = [NSString stringWithFormat:@"Day: %@ ℃", [f stringFromNumber:temp]];
    
    temp = dict[@"temp"][@"night"];
    temp = [NSNumber numberWithDouble:temp.doubleValue];
    self.tempLabelNight.text = [NSString stringWithFormat:@"Night: %@ ℃", [f stringFromNumber:temp]];

    NSNumber *timestamp = dict[@"dt"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp floatValue]];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"EEE, dd MMMM"];
    
    self.dateLabel.text = [dateformatter stringFromDate:date];
}

@end
