//
//  FirecastWeatherForCityTVC.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastWeatherForCityTVC : UITableViewController
@property (nonatomic, strong) City *city;
@end
