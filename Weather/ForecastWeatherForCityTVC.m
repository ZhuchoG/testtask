//
//  FirecastWeatherForCityTVC.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "ForecastWeatherForCityTVC.h"
#import "ForecastCell.h"

@interface ForecastWeatherForCityTVC ()
@property (nonatomic, strong) NSArray *weather;
@end

@implementation ForecastWeatherForCityTVC

- (NSArray *)weather
{
    if (!_weather)
    {
        _weather = [NSKeyedUnarchiver unarchiveObjectWithData:self.city.weather];
    }
    return _weather;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 60.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"dataUpdated" object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ForecastCell" bundle:nil] forCellReuseIdentifier:@"forecastCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.weather.count;
}

- (void)refresh
{
    self.weather = [NSKeyedUnarchiver unarchiveObjectWithData:self.city.weather];
    [self.tableView reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ForecastCell *cell = [tableView dequeueReusableCellWithIdentifier:@"forecastCell" forIndexPath:indexPath];
    
    NSDictionary *dayDict = self.weather[indexPath.row];
    
    if (!cell)
    {
        cell = [[ForecastCell alloc] init];
    }
    
    [cell updateWithDict:dayDict];
    
    return cell;
}

@end
