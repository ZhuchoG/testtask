//
//  SegmentedController.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SegmentedController : UIViewController
- (id)initWithKeys:(NSArray *)keys forControllers:(NSDictionary *)controllers;
@property (nonatomic, strong) NSDictionary *controllers;
@end
