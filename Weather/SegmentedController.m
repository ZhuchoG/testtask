//
//  SegmentedController.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "SegmentedController.h"

@interface SegmentedController ()

@property (nonatomic, strong) UISegmentedControl *switcher;
@property (nonatomic, strong) UIViewController *currentController;

@property (nonatomic, strong) NSArray *controllerNames;

@end

@implementation SegmentedController

- (id)initWithKeys:(NSArray *)keys forControllers:(NSDictionary *)controllers
{
    self = [super init];
    if (self) {
        self.controllerNames = keys;
        self.controllers = controllers;
    }
    return self;
}

- (UISegmentedControl *)switcher
{
    if (_switcher == nil)
    {
        _switcher = [[UISegmentedControl alloc] initWithItems:self.controllerNames];
    }
    
    return _switcher;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.switcher addTarget:self action:@selector(switchView:) forControlEvents:UIControlEventValueChanged];
    
    self.switcher.selectedSegmentIndex = 0;
    
    self.navigationItem.titleView = self.switcher;
    
    for (NSString *key in self.controllerNames)
    {
        UIViewController *vc = self.controllers[key];
        if (vc)
        {
            vc.view.frame = self.view.frame;
            [self addChildViewController:vc];
        }
    }
    
    UIViewController *firstVC = self.controllers[self.controllerNames[0]];
    if (firstVC)
    {
        [self.view addSubview:firstVC.view];
        self.currentController = firstVC;
    }
}

- (void)switchView:(UISegmentedControl *)sender
{
    if (self.currentController)
    {
        NSString *key = self.controllerNames[sender.selectedSegmentIndex];
        if (key)
        {
            UIViewController *newVC = self.controllers[key];
            if (newVC)
            {
                [self transitionFromViewController:self.currentController toViewController:newVC duration:0 options:UIViewAnimationOptionTransitionNone animations:^{} completion:nil];
                self.currentController = newVC;
            }
        }
    }
}

@end
