//
//  WeatherAPI.h
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherAPI : NSObject
+ (NSDictionary *)getCurrentDataForCityName:(NSString *)name;
+ (NSDictionary *)getForecastDataForCityName:(NSString *)name;
@end
