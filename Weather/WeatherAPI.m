//
//  WeatherAPI.m
//  Weather
//
//  Created by Vitaliy Zhukov on 16.05.15.
//  Copyright (c) 2015 Vitaliy Zhukov. All rights reserved.
//

#import "WeatherAPI.h"
#import "AppDelegate.h"

#define APIURL @"http://api.openweathermap.org/data/2.5"

@implementation WeatherAPI

+ (NSString *)encodeString:(NSString *)string fromEncoding:(NSStringEncoding)encoding
{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string,
                                                                                 NULL, (CFStringRef)@";/?:@&=$+{}<>,",
                                                                                 CFStringConvertNSStringEncodingToEncoding(encoding)));
}

+ (NSDictionary *)getDataForUrlString:(NSString *)urlString
{
    NSLog(@"%@", urlString);
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSError *error = nil;
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setNetworkActivityIndicatorVisible:YES];
    NSString *jsonString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setNetworkActivityIndicatorVisible:NO];
    
    if (!jsonString)
    {
        return @{@"error": @"Cannot get data"};
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *results = nil;
    
    if (jsonData)
    {
        results = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves|NSJSONReadingAllowFragments error:&error];
        if (!results)
        {
            return @{@"error": @"Cannot parse data"};
        }
    }
    
    return results;
}

+ (NSDictionary *)getCurrentDataForCityName:(NSString *)name
{
    return [self getDataForUrlString:[NSString stringWithFormat:@"%@/weather?units=metric&q=%@", APIURL, [self encodeString:name fromEncoding:NSUTF8StringEncoding]]];
}

+ (NSDictionary *)getForecastDataForCityName:(NSString *)name
{
    return [self getDataForUrlString:[NSString stringWithFormat:@"%@/forecast/daily?units=metric&cnt=7&q=%@", APIURL, [self encodeString:name fromEncoding:NSUTF8StringEncoding]]];
}

@end
